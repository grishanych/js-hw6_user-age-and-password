
function createNewUser() {
  let getFirstName = prompt('What is your name?');
  let getLastName = prompt('What is your lastname?');
  let getYourAge = '';
  while (!/^\d{2}\.\d{2}\.\d{4}$/.test(getYourAge)) {
    getYourAge = prompt('Enter your date of birth in the format dd.mm.yyyy:', getYourAge);
  }
  let newUser = {
    firstName: getFirstName,
    lastName: getLastName,
    birthday: getYourAge,
    getLogin() {
      let getFullName = this.firstName[0] + this.lastName;
      return getFullName.toLowerCase();
    },
    getAge() {
      const DATE = new Date();
      let dateOfBirth = this.birthday;
      let dateOfBirthReverse = dateOfBirth.replaceAll('.', '-').split('-').reverse().join('-');
      let yourBirthsday = new Date(dateOfBirthReverse)
      let yourAge = (DATE - yourBirthsday) / 1000 / 60 / 60 / 24 / 365.2;
      return Math.floor(yourAge);
    },
    getPassword() {
      let getPasswordString = this.firstName[0].toLocaleUpperCase() + this.lastName.toLocaleLowerCase() + this.birthday.slice(-4);
      return getPasswordString
    }

  };
  console.log(`You are: ${newUser.getAge()} years old`);
  console.log(`Your password: ${newUser.getPassword()}`);
  console.log(`Your login: ${newUser.getLogin()}`);
  return newUser;

}
createNewUser()